package _test

import (
	"bitbucket.org/lygo/lygo_scheduler"
	"fmt"
	"testing"
	"time"
)

func TestScheduler(t *testing.T) {

	sched := lygo_scheduler.NewScheduler()
	sched.AddSchedule(&lygo_scheduler.Schedule{
		Uid:      "every_10_second",
		StartAt:  "9:24:00",
		Timeline: "second:10",
		Payload: map[string]interface{}{
			"value":"HELLO PAYLOAD",
		},
	}, "arg1", "arg2")

	fmt.Println("configuration", sched)
	fmt.Println("timeout", sched.GetTimeout())
	sched.OnSchedule(func(schedule *lygo_scheduler.SchedulerTask) {
		fmt.Println("EVENT", schedule.Settings().Uid, schedule.Settings().StartAt, schedule.Settings().Timeline)
	})
	sched.OnError(func(err string) {
		fmt.Println("ERROR", err)
	})
	if sched.HasErrors() {
		fmt.Println("Parsing Errors", sched.GetErrors())
	}

	fmt.Println("START")
	sched.Start()

	time.Sleep(5 * time.Second)
	fmt.Println("RELOAD")
	sched.Reload()

	// toggle pause every 5 seconds
	go func() {
		for{
			time.Sleep(5 * time.Second)
			sched.TogglePause()
			fmt.Println("TOGGLE PAUSE:", sched.IsPaused())
		}
	}()

	fmt.Println("JOIN")
	sched.Join()
}

func TestSchedulerConfig(t *testing.T) {

	sched := lygo_scheduler.NewSchedulerFromFile("./scheduler.json")
	fmt.Println("configuration", sched)
	fmt.Println("timeout", sched.GetTimeout())
	sched.OnSchedule(func(schedule *lygo_scheduler.SchedulerTask) {
		fmt.Println("EVENT", schedule.Settings().Uid, schedule.Settings().StartAt, schedule.Settings().Timeline)
	})
	sched.OnError(func(err string) {
		fmt.Println("ERROR", err)
	})
	if sched.HasErrors() {
		fmt.Println("Parsing Errors", sched.GetErrors())
	}

	sched.Start()
	time.Sleep(5 * time.Second)
	fmt.Println("RELOAD")

	sched.Reload()

	time.Sleep(30 * time.Second)
}
