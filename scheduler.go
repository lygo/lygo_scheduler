package lygo_scheduler

import (
	"bitbucket.org/lygo/lygo_commons/lygo_io"
	"bitbucket.org/lygo/lygo_commons/lygo_json"
	"bitbucket.org/lygo/lygo_commons/lygo_regex"
	"bitbucket.org/lygo/lygo_commons/lygo_strings"
	"bitbucket.org/lygo/lygo_events"
	"fmt"
	"strings"
	"time"
)

//----------------------------------------------------------------------------------------------------------------------
//	c o n s t
//----------------------------------------------------------------------------------------------------------------------

const (
	onSchedule = "on_schedule"
	onError    = "on_error"
)

//----------------------------------------------------------------------------------------------------------------------
//	t y p e s
//----------------------------------------------------------------------------------------------------------------------

type SchedulerTaskHandler func(schedule *SchedulerTask)
type SchedulerErrorHandler func(err string)

type Scheduler struct {
	settings      *SchedulerSettings
	events        *lygo_events.Emitter
	taskHandlers  []SchedulerTaskHandler
	errorHandlers []SchedulerErrorHandler
	timer         *time.Ticker
	stopChan      chan bool
	closed        bool
	paused        bool
	tasks         []*SchedulerTask
}

//----------------------------------------------------------------------------------------------------------------------
//	c o n s t r u c t o r
//----------------------------------------------------------------------------------------------------------------------

func NewScheduler() *Scheduler {
	instance := new(Scheduler)
	instance.settings = new(SchedulerSettings)
	instance.events = lygo_events.NewEmitter()
	instance.taskHandlers = make([]SchedulerTaskHandler, 0)
	instance.errorHandlers = make([]SchedulerErrorHandler, 0)
	instance.stopChan = make(chan bool, 1)
	instance.closed = true
	instance.paused = false
	instance.tasks = make([]*SchedulerTask, 0)
	instance.initEvents()

	return instance
}

func NewSchedulerWithSettings(settings *SchedulerSettings) *Scheduler {
	instance := NewScheduler()
	instance.settings = settings

	return instance
}

func NewSchedulerFromFile(configFile string) *Scheduler {
	instance := NewScheduler()
	instance.load(configFile)

	return instance
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *Scheduler) Uid() string {
	if nil != instance && nil != instance.settings {
		return instance.settings.Uid
	}
	return ""
}

func (instance *Scheduler) String() string {
	if nil != instance {
		return instance.GoString()
	}
	return ""
}

func (instance *Scheduler) GoString() string {
	if nil != instance && nil != instance.settings {
		return lygo_json.Stringify(instance.settings)
	}
	return ""
}

func (instance *Scheduler) HasErrors() bool {
	if nil != instance {
		if len(instance.tasks) > 0 {
			for _, t := range instance.tasks {
				if nil != t.err {
					return true
				}
			}
		}
	}
	return false
}

func (instance *Scheduler) GetErrors() string {
	if nil != instance {
		builder := new(strings.Builder)
		if len(instance.tasks) > 0 {
			for _, t := range instance.tasks {
				if nil != t.err {
					builder.WriteString(t.err.Error() + "\n")
				}
			}
		}
		return builder.String()
	}
	return ""
}

func (instance *Scheduler) GetTimeout() time.Duration {
	if nil != instance {
		return instance.calculateTimeout()
	}
	return 0 * time.Second
}

func (instance *Scheduler) AddSchedule(item *Schedule, args ...interface{}) {
	if nil != instance {
		if len(args) > 0 {
			item.Arguments = append(item.Arguments, args...)
		}
		instance.settings.Schedules = append(instance.settings.Schedules, item)
	}
}

func (instance *Scheduler) AddScheduleByJson(json string, args ...interface{}) {
	if nil != instance {
		if lygo_regex.IsValidJsonObject(json) {
			var item Schedule
			err := lygo_json.Read(json, &item)
			if nil == err {
				instance.AddSchedule(&item, args...)
			}
		}
	}
}

func (instance *Scheduler) OnSchedule(handler SchedulerTaskHandler) {
	if nil != instance && nil != handler {
		instance.taskHandlers = append(instance.taskHandlers, handler)
	}
}

func (instance *Scheduler) OnError(handler SchedulerErrorHandler) {
	if nil != instance && nil != handler {
		instance.errorHandlers = append(instance.errorHandlers, handler)
	}
}

func (instance *Scheduler) Start() {
	if nil != instance && instance.closed {
		instance.closed = false
		if nil != instance.timer {
			instance.timer.Stop()
			instance.timer = nil
		}
		instance.initTasks()
		go instance.run()
	}
}

func (instance *Scheduler) Stop() {
	if nil != instance {
		if nil != instance.timer {
			instance.timer.Stop()
		} else {
			instance.timer = nil
		}
		instance.closed = true
		instance.stopChan <- true
		instance.stopChan = make(chan bool, 1) // reset channel
		instance.tasks = make([]*SchedulerTask, 0)
	}
}

func (instance *Scheduler) Pause() {
	if nil != instance {
		instance.paused = true
	}
}

func (instance *Scheduler) Resume() {
	if nil != instance {
		instance.paused = false
	}
}

func (instance *Scheduler) IsPaused() bool {
	if nil != instance {
		return instance.paused
	}
	return false
}

func (instance *Scheduler) TogglePause() {
	if nil != instance {
		instance.paused = !instance.paused
	}
}

func (instance *Scheduler) Reload() {
	if nil != instance {
		instance.Stop()
		instance.Start()
	}
}

func (instance *Scheduler) Join() {
	if nil != instance && nil != instance.stopChan && !instance.closed {
		<-instance.stopChan
	}
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func (instance *Scheduler) load(filename string) {
	if len(filename) > 0 {
		txt, err := lygo_io.ReadTextFromFile(filename)
		if nil == err && len(txt) > 0 && lygo_regex.IsValidJsonObject(txt) {
			_ = lygo_json.Read(txt, instance.settings)
		}
	}
}

func (instance *Scheduler) initEvents() {
	instance.events.On(onSchedule, func(event *lygo_events.Event) {
		if nil != instance && len(instance.taskHandlers) > 0 && !instance.closed && !instance.paused {
			item := event.Argument(0)
			if v, b := item.(*SchedulerTask); b {
				for _, handler := range instance.taskHandlers {
					if nil != handler && !instance.closed && !instance.paused {
						if instance.settings.Sync {
							handler(v)
						} else {
							go handler(v)
						}
					}
				}
			}
		}
	})
	instance.events.On(onError, func(event *lygo_events.Event) {
		if nil != instance && len(instance.errorHandlers) > 0 && !instance.closed {
			var err string
			item := event.Argument(0)
			if v, b := item.(string); b {
				err = v
			} else if v, b := item.(error); b {
				err = v.Error()
			}
			if len(err) > 0 {
				for _, handler := range instance.errorHandlers {
					if nil != handler && !instance.closed {
						handler(err)
					}
				}
			}
		}
	})
}

func (instance *Scheduler) initTasks() {
	// read configuration and creates task array
	schedules := instance.settings.Schedules
	for _, schedule := range schedules {
		task := NewSchedulerTask(instance.settings.Uid, schedule)
		instance.tasks = append(instance.tasks, task)
		if e := task.Error(); len(e) > 0 {
			instance.events.EmitAsync(onError, e)
		}
	}
}

func (instance *Scheduler) newTicker(fixed bool) *time.Ticker {
	if nil != instance {
		if fixed {
			return time.NewTicker(1 * time.Second)
		}
		return time.NewTicker(instance.calculateTimeout())
	}
	return nil
}

func (instance *Scheduler) calculateTimeout() time.Duration {
	if nil != instance {
		min := 1 * time.Minute
		for _, t := range instance.tasks {
			if t.timeline < min {
				min = t.timeline
			}
		}

		return min
	}
	return 1 * time.Second
}

func (instance *Scheduler) run() {
	instance.timer = instance.newTicker(true)
	if nil == instance.timer {
		return
	}
	for {
		select {
		case <-instance.stopChan:
			return // exit
		case <-instance.timer.C:
			// timer tick
			if nil != instance.timer {
				instance.timer.Stop()
				instance.timer = nil
				instance.checkSchedule()
				instance.timer = instance.newTicker(false)
				if nil == instance.timer {
					return
				}
			}
		}
	}
}

func (instance *Scheduler) checkSchedule() {
	if nil != instance && !instance.closed {
		// PANIC RECOVERY
		defer func() {
			if r := recover(); r != nil {
				// recovered from panic
				message := lygo_strings.Format("[panic] Scheduler '%s' ERROR: %s", instance.Uid(), r)
				if nil != instance && nil != instance.events {
					instance.events.EmitAsync(onError, message)
				} else {
					fmt.Println(message)
				}
			}
		}()

		for _, task := range instance.tasks {
			if task.IsReady() {
				// trigger
				instance.events.EmitAsync(onSchedule, task)
			}
		}
	}
}
